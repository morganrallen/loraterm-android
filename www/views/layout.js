var blelib = require("../ble")();
var html = require("choo/html");

module.exports = function(state, emit, content) {
  return html`
    <div>
      <div class="header">
        <div>Scanning: ${state.ble.scanning}</div>
        <div>Connecting: ${state.ble.connecting}</div>
        <div>Connected: ${state.ble.connected}</div>
        <div>ID: ${state.ble.connectedDeviceId()}</div>
      </div>
      <content>
        ${content}
      </content>
    </div>
  `;
}
