var app = require("choo")();
var html = require("choo/html");
var container = document.createElement("div");

var blelib = require("./ble")();

window.app = app;

app.use(blelib.store);
app.use(function(state, emitter) {
  emitter.on("ble:devices", function() {
  });
});

app.route("/", require("./views/scan"));
app.route("/device/:mac", require("./views/device"));

document.write('<script src="/cordova.js" type="text/javascript"></script>');
document.write('<link href="/css/index.css" type="text/css" rel="stylesheet" />');
document.write('<link href="/css/ble.css" type="text/css" rel="stylesheet" />');
document.body.appendChild(container);

// wait until cordova is ready to finish init'ing choo
document.addEventListener("deviceready", function() {
  app.mount(container);
}, false);
