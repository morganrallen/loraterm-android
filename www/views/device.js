var blelib = require("../ble")();
var html = require("choo/html");
var layout = require("./layout");

var SERVICES = {
  "4200": {
    "4201": "Bandwidth",
    "4202": "Coding Rate",
    "4203": "Spreading Factor",
    "4204": "Preamble",
    "4207": "Frequency",
  }
}

module.exports = function(state, emit) {
  if(!state.ble.connected && localStorage.deviceId && localStorage.autoConnect) {
    console.log("attempting auto connect");
    state.ble.connect(localStorage.deviceId, function(device) {
      console.log("connected", device);
    });
  } else if(!state.ble.connectedDeviceId()) {
    setTimeout(function() {
      emit("replaceState", "/");
      //emit("render");
    }, 100);
  }

  var device = state.ble.connectedDevice;

  return layout(state, emit, html`
    <div>
      ${serviceCount()}
      <div class="service-list">
        ${serviceList()}
      </div>
    </div>
  `);

  function serviceCount() {
    if(state.ble.connectedDevice)
      return html`<div>Services: ${device.services.length}</div>`;
    else return "";
  }

  function serviceList() {
    if(state.ble.connected) {
      var el = document.createElement("div");

      for(let i = 0; i < device.characteristics.length; i++) {
        let chr = device.characteristics[i];

        if(chr.properties.indexOf("Read") != -1) {
          console.log("reading %s", chr.service, chr.characteristic);
          ble.read(device.id, chr.service, chr.characteristic, function(buf) {
            if(buf.byteLength == 0) return;

            var label = SERVICES[chr.service] ? (SERVICES[chr.service][chr.characteristic] || chr.characteristic) : chr.characteristic;
            var value = new Uint8Array(buf, 0, 1);

            el.appendChild(html`<div><span>${label}: </span><span>${value[0]}</span></div>`);
          }, function() {
            console.error(chr.service, arguments);
          });
        }
      }

      return el;
    }
  }
};
