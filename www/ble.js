var html = require("choo/html");

module.exports = function() {
  return {
    store: function store(state, emitter) {
      state.ble = {
        connecting: false,
        connected: false,
        scanning: false,
        devices: [],

        connect: connect,

        connectedDeviceId: function() {
          if(state.ble["connectedDevice"]) {
            return state.ble.connectedDevice.id;
          } else {
            return null;
          }
        }
      };

      function connect(deviceId) {
        ble.stopScan();
        state.ble.scanning = false;
        state.ble.connecting = true;

        emitter.emit("render");

        ble.connect(deviceId, function(device) {
          console.log(device);

          state.ble.connected = true;
          state.ble.connecting = false;

          state.ble.connectedDevice = device;

          emitter.emit("render");
          emitter.emit("pushState", `/device/${device.id}`);

          localStorage.deviceId = device.id;
        }, function(device) {
          console.log("disconnect", device);

          state.ble.connecting = false;
          state.ble.connected = false;
          state.ble.connectedDevice = null;

          emitter.emit("render");
        });
      }
    },

    startScan: function(state, emit) {
      console.log("BLE: startScan");

      state.ble.devices = [];
      state.ble.connected = false;

      ble.startScan([], function(device) {
        state.ble.scanning = true;

        console.log(device);
        state.ble.devices.push(device);
        
        emit("ble:devices");
        emit("render");
      }, function(err) {
        state.ble.scanning = false;
        
        emit("render");
      });
    },

    deviceList: function(state, emit) {
      return html`<div class="device-list">${deviceList()}</div>`;

      function deviceList() {
        return state.ble.devices.map(function(device) {
          return html`<div class="device">
                        <span>${device.name || device.id }</span>
                        <button onclick=${state.ble.connect(device.id)} class="connect">+</button></div>
                     `;

        });
      }
    }
  }
}
