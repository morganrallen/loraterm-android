var blelib = require("../ble")();
var html = require("choo/html");
var layout = require("./layout");

module.exports = function(state, emit) {
  return layout(state, emit, html`
    <div class="scan-control">
      <button class="scan" onclick=${blelib.startScan.bind({}, state, emit)}>Scan</button>
      ${blelib.deviceList(state, emit)}
    </div>
  `);
}
